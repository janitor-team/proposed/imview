/*------------------------------------------------------------------------
 *
 * The top-level code for point annotation.
 *
 * Hugues Talbot	13 Jan 2001
 *
 * Following discussions with PSL. 
 *      
 *-----------------------------------------------------------------------*/

#include "imview.hxx"
#include "annotatePoints.hxx"
#include "pointfile.hxx"

extern pointfile    *PtFileMngr;

annotatept::annotatept()
{
    dbgprintf("Point annotation dialog created\n");
    annotateWindow = 0; // fluid depends on this
    return;
}

annotatept::~annotatept()
{
    dbgprintf("Point annotation dialog destroyed\n");
    return;
}

void annotatept::setDefaults()
{
    return;
}

void annotatept::show()
{
    annotateWindow->show();
}

void annotatept::hide()
{
    annotateWindow->hide();
}

void annotatept::getPointParams(void)
{
    int          font, size, length;
    string       ann;
    Fl_Color     col;
    str_quadrant q;
    bool         h;
    int          ptsize;
    
	if (PtFileMngr->getNearestPointParameters(X_,Y_, ann, col, font, size, length, q, h, ptsize)) {
		setAnnotation(ann.c_str());
		fontChoice->value(font);
		fontColourChoice->value(col);
		fontSizeSlider->value(size);
		distanceValue->value(length);
		fontQuadrant = q;
		hide(h);
		setPointSize(ptsize);
	}
    // else do nothing.
}

// the annotatept callbacks

void setdistance_cb(Fl_Roller *r, annotatept *panel)
{
    panel->setDistance((int)(r->value())); // forward value to Value holder
}

void cancelbutton_cb(Fl_Button *b, annotatept *panel)
{
    panel->hide();
}

void okbutton_cb(Fl_Return_Button *b, annotatept *panel)
{
    int    xx, yy;
    
    panel->getXY(xx,yy);
    PtFileMngr->annotatePoint(xx, yy);
    
    panel->hide();
}

void annotateinput_cb(Fl_Input *, annotatept *)
{
    return;
}

void SE_cb(Fl_Round_Button *, annotatept *panel)
{
    panel->setFontQuadrant(STR_SE);
}

void NE_cb(Fl_Round_Button *, annotatept *panel)
{
    panel->setFontQuadrant(STR_NE);
}

void NW_cb(Fl_Round_Button *, annotatept *panel)
{
    panel->setFontQuadrant(STR_NW);
}

void SW_cb(Fl_Round_Button *, annotatept *panel)
{
    panel->setFontQuadrant(STR_SW);
}


