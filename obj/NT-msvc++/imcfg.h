/* imcfg.h.  Generated automatically by configure.  */
/* config.h.in.  Generated automatically from configure.in by autoheader.  */
/* Hacked by hand by Hugues Talbot	 8 Aug 2000 */

/* Guards */

#ifndef CONFIG_H
#define CONFIG_H

/* these are my own! Hugues Talbot	 7 Aug 2000 */

#if defined(WIN32) && !defined(__CYGWIN__)
#	define WIN32_NOTCYGWIN 1
#else
#	undef  WIN32_NOTCYGWIN
#endif

/* imview version */
#define IMVIEW_MAJOR 1
#define IMVIEW_MINOR 0
#define IMVIEW_MICRO 1

/* Define if you have the Magick library (-lMagick).  */
/*
#define HAVE_MAGICK 1 
*/

/* Define if if ImageMagick uses the new exception structure */
#define MAGICK_USES_REASON 

/* Define if if ImageMagick uses InitializeMagick */
/* #undef MAGICK_USES_INIT */

/* Define if you have the jpeg library (-ljpeg).  */
#define HAVE_JPEG 1

/* Define if you have the pthread library (-lpthread).  */
#define PtW32NoCatchWarn /* shut the bloody extra warning up */
#define HAVE_PTHREADS 1

/* Define if we only really have DEC threads, precursors to pthreads */
/* #undef HAVE_DECTHREADS */

/* Define if you have the tiff library (-ltiff).  */
#define HAVE_TIFF 1

/* define if you have the Unix SYSV IPC */
/* #define HAVE_SYSV 1 */ 

/* Define if you have the GL library (-lGL).  */
/* #undef HAVE_GL */

/* stupid C++ ANSI compatibility issues */
/* define if your compiler supports class member templates */
/* #define HAVE_MEMBER_TEMPLATES */

/* define if one can have function template without a template argument */
/* #define HAVE_TEMPLATE_KEYWORD_QUALIFIER */ 

/* define if your compiler rejects array casts in templates */
/* #define HAVE_TEMPLATE_CAST_ARRAY */

/* define if the compiler knows about standard char traits */
#define HAVE_STANDARD_CHAR_TRAITS 


/* CAREFUL, for these the *VALUE* is used, not the definition! */
/* define if you have scandir */
#define HAVE_SCANDIR 1

/* define if you have snprintf */
#define HAVE_SNPRINTF 1

/* define if you have vsnprintf */
#define HAVE_VSNPRINTF 1

/* sleep functions */
/*
#define HAVE_USLEEP 1
#define HAVE_NANOSLEEP 1
*/

/* Define the default installation path */
#define PrefPath "C:/progra~1/imview"

/* these were generated by autoheader (I think...) */

/* Define to empty if the keyword does not work.  */
/* #undef const */

/* Define as the return type of signal handlers (int or void).  */
#define RETSIGTYPE void

/* Define if the setvbuf function takes the buffering type as its second
   argument and the buffer pointer as the third, as on System V
   before release 3.  */
/* #undef SETVBUF_REVERSED */

/* Define to `unsigned' if <sys/types.h> doesn't define.  */
/* #undef size_t */

/* Define if you have the ANSI C header files.  */
#define STDC_HEADERS 1

/* Define if you can safely include both <sys/time.h> and <time.h>.  */
#define TIME_WITH_SYS_TIME 1

/* Define if your <sys/time.h> declares struct tm.  */
/* #undef TM_IN_SYS_TIME */

/* Define if you have the getcwd function.  */
#define HAVE_GETCWD 1

/* Define if you have the select function.  */
/*
#define HAVE_SELECT 1
*/

/* Define if you have the socket function.  */
#define HAVE_SOCKET 1

/* Define if you have the strdup function.  */
#define HAVE_STRDUP 1

/* Define if you have the strerror function.  */
#define HAVE_STRERROR 1

/* Define if you have the strstr function.  */
#define HAVE_STRSTR 1

/* Define if you have the strtol function.  */
#define HAVE_STRTOL 1

/* Define if you have the uname function.  */
#define HAVE_UNAME 1

/* Define if you have the <dirent.h> header file.  */
#define HAVE_DIRENT_H 1

/* Define if you have the <fcntl.h> header file.  */
#define HAVE_FCNTL_H 1

/* Define if you have the <limits.h> header file.  */
#define HAVE_LIMITS_H 1

/* Define if you have the <malloc.h> header file.  */
#define HAVE_MALLOC_H 1

/* Define if you have the <ndir.h> header file.  */
/* #undef HAVE_NDIR_H */

/* Define if you have the <sys/dir.h> header file.  */
/* #undef HAVE_SYS_DIR_H */

/* Define if you have the <sys/ndir.h> header file.  */
/* #undef HAVE_SYS_NDIR_H */

/* Define if you have the <sys/time.h> header file.  */
/*
#define HAVE_SYS_TIME_H 1
*/

/* Define if you have the <unistd.h> header file.  */
#define HAVE_UNISTD_H 1

/* Define if you don't have vprintf but do have _doprnt.  */
/* #undef HAVE_DOPRNT */

/* Define if you have the vprintf function.  */
#define HAVE_VPRINTF 1

#endif // CONFIG_H
