/*------------------------------------------------------------------------
 *
 * Prototypes for the minimal version of the
 * Library of Image Analysis Routines
 *
 * Hugues Talbot	 4 Jan 2001
 *      
 *-----------------------------------------------------------------------*/

int LIARdebug(char *fmt, ...);
