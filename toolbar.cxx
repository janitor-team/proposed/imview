/*
 * $Id: toolbar.cxx,v 4.5 2004/06/24 05:08:45 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Most of imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */


/*------------------------------------------------------------------------
 *
 * A simple toolbar, for now.
 *
 * Hugues Talbot	29 Jan 2001
 *      
 *-----------------------------------------------------------------------*/

#include "imview.hxx"
#include "toolbar.hxx"
#include "imageIO.hxx"
#include "imageViewer.hxx"
#include "menubar.hxx" // rotations, etc

extern imageIO     *IOBlackBox;
extern imageViewer *mainViewer;

tipButton::tipButton(int x, int y, int w, int h, const char *label)
    : Fl_Button(x,y,w,h,label)
{
    dbgprintf("TipButton constructed\n");
    message_ = 0;
    parentToolbar_ = 0;
    return;
}

tipButton::~tipButton()
{
}

int tipButton::handle(int event)
{
    int retval = 0;

    switch (event) {
      case FL_ENTER:
	chg_cursor(FL_CURSOR_CROSS);
	dbgprintf("My message is: %s\n", message_);
	//if (parentToolbar_)
	//    parentToolbar_->setMessage(message_);
	retval = 1;
	break;

	// just relying on FL_ENTER is not quite good enough
	// when buttons are close together it may be that
	// events are not delivered in the desired order
      case FL_MOVE:
	chg_cursor(FL_CURSOR_CROSS);
	retval = 1; 
	break;

      case FL_LEAVE:
	chg_cursor(FL_CURSOR_DEFAULT);
	retval = 1;
	break;

	// call the parent class method.
      default:
	retval = Fl_Button::handle(event);
	break;
    }

    return retval;
}

void tipButton::chg_cursor(Fl_Cursor c)
{
    Fl_Window *w = window();
    w->make_current();
    fl_cursor(c);
}

toolbar::toolbar()
{
    dbgprintf("Toolbar created\n");
    toolbarWindow = 0;
    return;
}


toolbar::~toolbar()
{
    dbgprintf("Toolbar destroyed\n");
    return;
}

void toolbar::setDefaults()
{
    return;
}

/* -- All the callbacks -- */

void setpointmode_cb(Fl_Choice *b, toolbar *tb)
{
    dbgprintf("Choice value: %d\n", b->value());
    if (mainViewer) {
        switch (b->value()) {
        case 0:
            dbgprintf("Add point mode\n");
            mainViewer->setpointermode(IMV_POINTER_ADDPT_MODE);
            break;
            
        case 1:
            dbgprintf("Select point mode\n");
            mainViewer->setpointermode(IMV_POINTER_SELECT_MODE);
            break;
            
        case 2:
            dbgprintf("Draw points mode\n");
            mainViewer->setpointermode(IMV_POINTER_DRAWPT_MODE);
            break;

        case 3:
            dbgprintf("Delete single point mode\n");
            mainViewer->setpointermode(IMV_POINTER_DELPT_MODE);
            break;

        case 4:
            dbgprintf("Delete point group mode");
            mainViewer->setpointermode(IMV_POINTER_DELGRP_MODE);
            break;
        }
    }
    return;
}

void setzoommode_cb(tipButton *, toolbar *tb)
{
    dbgprintf("Set Zoom Mode callback\n");
    if (mainViewer)
	mainViewer->setpointermode(IMV_POINTER_ZOOM_MODE);
}

void setmeasuremode_cb(tipButton *, toolbar *tb)
{
    dbgprintf("Set Measure Mode callback\n");
    if (mainViewer)
	mainViewer->setpointermode(IMV_POINTER_MEASURE_MODE);
}

void setselectmode_cb(tipButton *, toolbar *)
{
    dbgprintf("Set Select Mode callback\n");
    if (mainViewer)
	mainViewer->setpointermode(IMV_POINTER_SELECT_MODE);
}

void addpointmode_cb(Fl_Widget *,  void *)
{
    dbgprintf("Add Point Mode callback\n");
    if (mainViewer)
	mainViewer->setpointermode(IMV_POINTER_ADDPT_MODE);
}

void addpointmode_cb(tipButton *, toolbar *tb)
{
    dbgprintf("Add Point Mode callback\n");
    if (mainViewer)
	mainViewer->setpointermode(IMV_POINTER_ADDPT_MODE);
}

void drawpointmode_cb(tipButton *, toolbar *tb)
{
    dbgprintf("Draw Point Mode callback\n");
    if (mainViewer)
	mainViewer->setpointermode(IMV_POINTER_DRAWPT_MODE);
}


void deletepointmode_cb(tipButton *, toolbar *tb)
{
    dbgprintf("Delete Point Mode callback\n");
    if (mainViewer)
	mainViewer->setpointermode(IMV_POINTER_DELPT_MODE);
}

void editpointmode_cb(tipButton *, toolbar *tb)
{
    dbgprintf("Edit Point Mode callback\n");
    if (mainViewer)
	mainViewer->setpointermode(IMV_POINTER_EDITPT_MODE);
}

void hidetoolbar_cb(tipButton *, toolbar *tb)
{
  dbgprintf("Hide Toolbar callback\n");
  tb->hide();
}


// relay the callback
void local_rotate90right_cb(tipButton *b, void *)
{
    if(IOBlackBox->imageData() != 0)
	((Fl_Callback *)rotate90right_cb)(0,0);
}

void local_rotate90left_cb(tipButton *b, void *)
{
    if(IOBlackBox->imageData() != 0)
	((Fl_Callback *)rotate90left_cb)(0,0);
}

void local_flipv_cb(tipButton *b, void *)
{
    if(IOBlackBox->imageData() != 0)
	((Fl_Callback *)flipv_cb)(0,0);
}

void local_fliph_cb(tipButton *b, void *)
{
    if(IOBlackBox->imageData() != 0)
	((Fl_Callback *)fliph_cb)(0,0);
}

