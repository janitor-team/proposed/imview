/*
 * $Id: asynchat.hxx,v 4.2 2007/06/07 13:30:19 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

// -*- Mode: C++; tab-width: 4 -*-

// ======================================================================
// Copyright 1996 by Sam Rushing
//
//                         All Rights Reserved
//
// Permission to use, copy, modify, and distribute this software and
// its documentation for any purpose and without fee is hereby
// granted, provided that the above copyright notice appear in all
// copies and that both that copyright notice and this permission
// notice appear in supporting documentation, and that the name of Sam
// Rushing not be used in advertising or publicity pertaining to
// distribution of the software without specific, written prior
// permission.
//
// SAM RUSHING DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
// INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN
// NO EVENT SHALL SAM RUSHING BE LIABLE FOR ANY SPECIAL, INDIRECT OR
// CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
// OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
// NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
// CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
// ======================================================================

#ifndef ASYNCHAT_H
#define ASYNCHAT_H

#include <string>
#include <list>
#include <queue>
#include <algorithm>
#include "imnmspc.hxx" // namespace def, if any req.
#include "asyncore.hxx"

using std::list;
//using std::queue;

// ===========================================================================
// producer and simple_producer
// ===========================================================================

class producer
{
public:
    virtual ~producer()
        { /* necesary */
        }
    virtual string* more (void) = 0;
};

class simple_producer : public producer
{
    string data;

public:
    simple_producer (const string& output_string)
        {
            data = output_string;
        }
    string* more (void)
        {
            string * slice = new string (data.substr (0,512));
            int l = data.length();
            // some string implementations are more fragile than others
            if (l > 512)
                data.erase (0,512);
            else if (l > 0)
                data.erase(); // the whole string
            return slice;
        }
};

// ===========================================================================
// async_chat
// ===========================================================================

class async_chat : public dispatcher
{
    string ac_in_buffer;
    string ac_out_buffer;
    string terminator;

    std::queue<producer*> producer_fifo;
    static const unsigned int ac_in_buffer_size;
    static const unsigned int ac_out_buffer_size;
    static const string null_terminator;
    inline int find_prefix_at_end (string haystack, string needle);


public:
    virtual         ~async_chat(void)
        { /* necessary */
        }

    virtual void set_terminator   (const string & t);
    virtual string& get_terminator      (void);
    virtual void collect_incoming_data (const string& data)
        { }
    ;
    virtual void found_terminator  (void)
        { }
    ;
    virtual void send     (const string& data);
    virtual void send     (producer* p);

    void   handle_read    (void);
    void   handle_write   (void);
    bool   readable    (void);
    bool   writable    (void);
    void   refill_buffer   (void);
    void   initiate_send   (void);
    void   close_when_done   (void)
        {
            producer_fifo.push ((producer *) 0);
        }

};

#endif // ASYNCHAT_H
