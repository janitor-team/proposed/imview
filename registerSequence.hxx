/*
 * $Id: registerSequence.hxx,v 1.1 2009/05/07 23:59:10 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*------------------------------------------------------------------------
 *
 * A class associated with a panel for reading arbitrary uncompressed
 * images
 *
 * Hugues Talbot	 8 Apr 2000
 * 
 *      
 *-----------------------------------------------------------------------*/

#ifndef REGISTERPANEL_H
#define REGISTERPANEL_H

#include <sstream>
#include <FL/Fl.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Int_Input.H>
#include <FL/Fl_Output.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Box.H>

#include "imview.hxx"

class registerpanel {
public:
    registerpanel();
	~registerpanel();
    void setDefaults(void);
    void show(void);
    void hide(void);
    // access to private data
    void setxmin(int v) {xmin = v;}
    void setymin(int v) {ymin = v;}
    void setxmax(int v) {xmax = v;}
	void setymax(int v) {ymax = v;}
	void setend(int v) {end = v;}
	void setstart(int v) {start = v;}

//////////////////
	void registration();
///////////////////

    friend Fl_Window *registration_panel(registerpanel &ri);

private:
    // Dialog window
    Fl_Window         *registerWindow;
    //
    Fl_Box            *titleBox;
    // coordinates of the selection
    Fl_Int_Input      *xminInput, *yminInput, *xmaxInput, *ymaxInput;
    // Temporal frame
	Fl_Int_Input      *tfstartInput, *tfendInput;
    // i/o
    Fl_Input      *io_outInput, *io_Input;

    // usual buttons
    Fl_Button         *helpButton, *cancelButton, *OKButton;

    // ------- other bits
    int               xmin, ymin, xmax, ymax;
    int               start, end;

///////////////////////////////////////

/////////////////////////////////
};

#endif // REGISTERPANEL_H
