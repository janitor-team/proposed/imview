/*
 * $Id: imunistd.h,v 4.3 2009/01/26 14:39:43 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*------------------------------------------------------------------------
 *
 * Compatibility Unistd header, not found with windows.
 *      
 *-----------------------------------------------------------------------*/

#ifndef IMUNISTD_H
#define IMUNISTD_H

#include <imcfg.h>

#ifdef HAVE_UNISTD_H
#  include <unistd.h>
#else
#  include <io.h>
#  include <direct.h>
#  define sleep _sleep
#  ifdef _MSC_VER // Visual C++
#    define strcasecmp(s,t)	stricmp((s), (t))
#    define strncasecmp(s,t,n)	strnicmp((s), (t), (n))
#    define access _access
#    define F_OK   00  // check for existence
#    define W_OK   02  // write permission
#    define R_OK   04  // check for read access
     /* extras.. */
#    include <windows.h>
     typedef int ssize_t;
#    define snprintf _snprintf
#    define vsnprintf _vsnprintf
#  endif // _MSC_VER

#endif // WIN32_NOTCYGWIN
#endif // IMUNISTD_H
