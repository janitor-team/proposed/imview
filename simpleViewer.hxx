/*
 * $Id: simpleViewer.hxx,v 4.1 2005/03/03 14:45:35 hut66au Exp $
 *
 * Imview, The Portable Image Analysis Application
 * Http://Www.Cmis.Csiro.Au/Hugues.Talbot/Imview
 * ----------------------------------------------------------
 *
 *  Imview Is An Attempt To Provide An Image Display Application
 *  Suitable For Professional Image Analysis. It Was Started In
 *  1997 And Is Mostly The Result Of The Efforts Of Hugues Talbot,
 *  Image Analysis Project, Csiro Mathematical And Information
 *  Sciences, With Help From Others (See The Credits Files For
 *  More Information)
 *
 *  Imview Is Copyrighted (C) 1997-2005 By The Australian Commonwealth
 *  Science And Industry Research Organisation (Csiro). Please See The
 *  Copyright File For Full Details. Imview Also Includes The
 *  Contributions Of Many Others. Please See The Credits File For Full
 *  Details.
 *
 *  This Program Is Free Software; You Can Redistribute It And/Or Modify
 *  It Under The Terms Of The Gnu General Public License As Published By
 *  The Free Software Foundation; Either Version 2 Of The License, Or
 *  (At Your Option) Any Later Version.
 *  
 *  This Program Is Distributed In The Hope That It Will Be Useful,
 *  But Without Any Warranty; Without Even The Implied Warranty Of
 *  Merchantability Or Fitness For A Particular Purpose.  See The
 *  Gnu General Public License For More Details.
 *  
 *  You Should Have Received A Copy Of The Gnu General Public License
 *  Along With This Program; If Not, Write To The Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, Ma 02111, Usa.
 * */

/*------------------------------------------------------------------------
 *
 *  A simpler viewer class, suitable for the 3D viewer
 *
 *
 *
 *-----------------------------------------------------------------------*/

#ifndef SIMPLEVIEWER_H
#define SIMPLEVIEWER_H

#include <FL/Fl.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Image.H>
#include <FL/fl_draw.H>

#include "imview.hxx"
#include "imageIO.hxx"

typedef enum {simpleview_top=0,
              simpleview_left,
              simpleview_bottom} SIMPLEVIEW;

class simpleViewer: public Fl_Group {
public:
    simpleViewer(int x, int y, int w, int h, const char *label=0);
    ~simpleViewer();
    // has to be implemented
    void draw();
    
private:
    void zapImageObject(void);
    
private:
    Fl_RGB_Image *imageObject;
    imageIO      *theImage;
    SIMPLEVIEW    whichView;
    
};

#endif // SIMPLEVIEWER_H
