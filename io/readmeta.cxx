/*
 * $Id: readmeta.cxx,v 1.4 2009/01/26 14:39:44 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/* 
 * 	readmeta ()  - reads in ITK Meta Images
 *
 *      Hugues Talbot	12 Dec 2007
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string> // STL
#include <iostream>
#include <fstream>
#include <sstream>
#include <fcntl.h>
#include "imunistd.h"
#include "imview.hxx"
#include "nocase.hxx"
#include "imageIO.hxx"
#include "imtranslate.hxx"
#include "readraw.hxx"
#include "readmeta.hxx"

#define TRUE 1
#define FALSE 0

extern imageIO *IOBlackBox;

/*--declarations of locally defined functions--*/


int readMETAImage(const char *name, int index)
{
    void   **buf;
    RAWFILE_HEADER myHeader;
    int res;
    
    // call the low-level META reader

    res = readmetadata(name, myHeader, buf);

    
    if (res == 0) {
        IOBlackBox->setCurrBuffp(buf);
        IOBlackBox->setCurrImgWidth(myHeader.nx);
        IOBlackBox->setCurrImgHeight(myHeader.ny);
        IOBlackBox->setCurrImgThickness(myHeader.nz);
        IOBlackBox->setXOffset(0);
        IOBlackBox->setYOffset(0);
        IOBlackBox->setZOffset(0);
        IOBlackBox->setCurrZPos(0); // first plane
        // only guessing there.
        if (myHeader.spp > 1)
            IOBlackBox->setCurrImgType(IM_SPECTRUM);
        else
            IOBlackBox->setCurrImgType(IM_SINGLE);
        IOBlackBox->setCurrPixType(myHeader.pixt);
        IOBlackBox->setCurrImgNbComps(1);
        IOBlackBox->setCurrImgNbSamples(myHeader.spp);
        IOBlackBox->setImgDesc("META");
        // now that we swap the byte order on demand,
        // we need to read the byte order
    }	
	
    return res;
}

// returns the number of frames in an ICS file
int metanbsubfiles(const char *)
{
    return 1; // that was easy.
}

static string trimlastblanks(string &s)
{
    string ret;
    string::size_type pos;
    if ((pos = s.find_first_of(" ")) != string::npos) {
        ret = s.substr(0,pos);
    } else {
        ret = s;
    }
    return ret;
}

static string trimfirstblanks(string &s)
{
    string ret;
    string::size_type pos;
    if ((pos = s.find_first_not_of(" ")) != string::npos) {
        ret = s.substr(pos);
    } else {
        ret = s;
    }
    return ret;
}

static int parse_meta_header(const char *filename,	 /* input filename  */
		    RAWFILE_HEADER  &myHeader)	 /* RAW header      */
{
/** Reads the META header, fills a RAW header

    RETURN VALUE:	int 

    DESCRIPTION:
    Read the the ITK Meta header file into the dataset description
    structure.
    
    HISTORY:
    Hugues Talbot, ESIEE 2007

    TESTS:

    REFERENCES:

    KEYWORDS:

**/
    int nbdims = 0;
    int nx, ny, nz, skip;
    pixtype ptype=IM_INVALID;
    endianness endian=ENDIAN_LITTLE;
    bool retval = false;
    // we will be using the data here outside the function.
    static string externalFile;    
    
    std::ifstream headerfile(filename);
    if (headerfile) {
        string currentline;
        string::size_type pos;
        dbgprintf("Loading header for ITK Meta image %s\n", filename);
        do {
            getline(headerfile, currentline);

            // look for the = sign
            if ((pos = currentline.find("=")) != string::npos) {
                string tmpkey = currentline.substr(0,pos);
                string key = trimlastblanks(tmpkey);
                string tmpval = currentline.substr(pos+1);
                string value = trimfirstblanks(tmpval);

                dbgprintf("Key=<%s>, value = <%s>\n", key.c_str(), value.c_str());

                if (cmp_nocase(key, "ndims") == 0) {
                    nbdims = atoi(value.c_str());
                    dbgprintf("Nb of dimensions = %d\n", nbdims);
                    continue;
                }
                if (cmp_nocase(key, "dimsize") == 0)  {
                    std::istringstream isk(value);
                    nx = ny = nz = 1;
                    if (nbdims >= 3) {
                        isk >> nx >> ny >> nz;
                    } else if (nbdims == 2) {
                        isk >> nx >> ny;
                    } else {
                        dbgprintf("Cannot read dimensions\n");
                        retval = 1;
                        break;
                    }
                    dbgprintf("Dimensions : nx = %d, ny = %d, nz=%d\n", nx,ny,nz);
                    continue;
                }
                if (cmp_nocase(key, "elementtype") == 0) {
                    ptype = IM_INVALID;
                    if (cmp_nocase(value, "met_uchar") == 0)
                        ptype = IM_UINT1;
                    if (cmp_nocase(value, "met_ushort") == 0)
                        ptype = IM_UINT2;
                    if (cmp_nocase(value, "met_short") == 0)
                        ptype = IM_INT2;
                    if (cmp_nocase(value, "met_int") == 0)
                        ptype = IM_INT4;
                    if (cmp_nocase(value, "met_float") == 0)
                        ptype = IM_FLOAT;
                    if (cmp_nocase(value, "met_double") == 0)
                        ptype = IM_DOUBLE;
                    dbgprintf("Pixel type : %s\n", IOBlackBox->typeName(ptype));
                    continue;
                }

                if (cmp_nocase(key, "binarydatabyteordermsb") == 0) {
                    endian = ENDIAN_LITTLE;
                    if (cmp_nocase(value, "true") == 0) {
                        endian = ENDIAN_BIG;
                    }
                    dbgprintf("Endianness : %d\n", (int)endian);
                }
                
                if (cmp_nocase(key, "elementdatafile") == 0) {
                    // does this key point to a different file
                    if ((value.length() != 0) && (cmp_nocase(value, "LOCAL") != 0)) {
                        externalFile = value; // full copy
                    } else {
                        externalFile.clear(); // empty filename
                    }
                    // at any rate, the_end
                    dbgprintf("End of header\n");
                    break; 
                }
                dbgprintf("Unknown key !\n");
            } 
        }  while (!currentline.empty());

        if (externalFile.length() == 0) {
            // where are we in the file ?
            skip = headerfile.tellg();
            dbgprintf("We need to skip %d bytes\n", skip);
        } else {
            dbgprintf("The data is in external file %s, we don't need so skip\n",
                      externalFile.c_str());
            skip = 0;
        }
        headerfile.close();

        // fill the header
        if (retval == 0) {
            myHeader.nx = nx;
            myHeader.ny = ny;
            myHeader.nz = nz;
            myHeader.spp = 1;
            myHeader.byo = endian;
            myHeader.itl = 0;
            myHeader.pixt = ptype;
            myHeader.skip = skip;
            if (externalFile.length() == 0)
                myHeader.externalData = NULL;
            else
                myHeader.externalData = externalFile.c_str();
        }
    }

    return retval;
}

/*---------------------------------------------------------------------------*/


  
/*---------------------------------------------------------------------------*/
/* front-end code for the LIAR, Hugues Talbot	18 Dec 1997 */

int readmetadata(const char *fname, RAWFILE_HEADER  &myHeader, void **&buf)
{
    int res = 0;
    if ((res = parse_meta_header(fname, myHeader)) == 0) {
        if (myHeader.externalData == NULL)
            res = readrawdata(fname, myHeader, 0, buf);
        else {
            const char *p = myDirName(fname);
            char fullpath[DFLTSTRLEN];
            snprintf(fullpath, DFLTSTRLEN,"%s/%s", p, myHeader.externalData);
            dbgprintf("Reading data image from %s\n", fullpath);
            res = readrawdata(fullpath, myHeader, 0, buf);
        }
    }
    return res;
}

