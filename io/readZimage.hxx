/*
 * $Id: readZimage.hxx,v 4.1 2007/11/19 18:17:55 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*------------------------------------------------------------------------
 *
 * The Z-IMAGE image format reader.
 *
 * Hugues Talbot	14 Jan 1998
 *      
 *-----------------------------------------------------------------------*/

#ifndef READZIMAGE_H
#define READZIMAGE_H

#ifndef trivmin
#define trivmin(x,y) ((x) > (y)) ? (y):(x)
#endif


int readZImage(const char *name,
	       int whichComponent = 0);  // a Zimage file may have more that one image in it.

int readZOverlay(const char *name);

int load_zimage(const char *fname,	
		int         imageindex,	
		int         start[3],	
		int         end[3],	
		int        *pixtype,	
		int        *imgtype,	
		int        *spp,        
		int        *nbcomp,     
		void      **inbuffp);	

int zimagenbsubfiles(const char *fname);

// public global function (badbadbad)
endianness checkendian(void);
void swapintbuf(int *buf, long buflen);
void swapdoublebuf(double *buf, long buflen);

#endif // READZIMAGE_H
