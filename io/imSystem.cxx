/*
 * $Id: imSystem.cxx,v 4.0 2003/04/28 14:44:33 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */


/*------------------------------------------------------------------------
 *
 * Some wrappers for system functions
 *
 * Hugues Talbot	29 Jul 1998
 *      
 *-----------------------------------------------------------------------*/


#include <stdio.h>

static bool isStd = false;

FILE *im_fopen(const char *path, const char *mode)
{
    if (path[0] == '|') {
	isStd = true;
	if (mode[0] == 'r')
	    return stdin;
	else
	    return stdout;
    } else {
	isStd = false;
	return fopen(path, mode);
    }
}

int im_fclose (FILE *stream)
{
    if (!isStd)
	return fclose(stream);
    else
	return 0;
}


// the assumption here is that there will always be only
// one temporary file made from stdin...

extern char *imTempFileName; // declared in imview.C

int stdinToTemp(void)
{
    FILE *fp;
    int   i;
    
    imTempFileName = tempnam(0, "flim");

    if ((fp = im_fopen(imTempFileName, "w")) == 0) {
	return 10;
    } else {
	/* copy the data */
	while ( (i=getchar()) != EOF) putc(i,fp);
	fclose(fp);
    }

    return 0;
}

