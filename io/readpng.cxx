/*
 * $Id: readpng.cxx,v 4.4 2008/01/22 13:54:22 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://imview.sourceforge.net
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2007 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*------------------------------------------------------------------------
 *
 * Read PNG natively.
 * A very important codec.
 *
 * This code shamelessly taken from FLTK.
 * 
 * Hugues Talbot	 9 Jun 2007
 *
 *      
 *-----------------------------------------------------------------------*/

#include "imnmspc.hxx" // name space definition if needed 

#include <stdio.h>
#include <stdlib.h>
//#include <setjmp.h>
#include "imview.hxx"

#if defined(HAVE_PNG) && defined(HAVE_LIBZ)
#  include <zlib.h>
#  include <png.h>
#endif // HAVE_LIBPNG && HAVE_LIBZ

#include "imageIO.hxx"


extern imageIO *IOBlackBox;


int pngnbcomp(const char *ident)
{
    return 1; // we need to use MNG if we want movies
}

// read a PNG image, the index is always ignored.
// Limitations :
//  - we do not read 16-bit images correctly (they get cast to 8-bit)
//  - we do not yield separate buffers per components.
//
int readPNGImage(const char *pngfilename, int index) // I - File to read
{
    int         retval = 0;
#if defined(HAVE_PNG) && defined(HAVE_LIBZ)
    int		i;			// Looping var
    FILE		*fp;			// File pointer
    int		channels;		// Number of color channels
    png_structp	pp;			// PNG read pointer
    png_infop	info;			// PNG info pointers
    png_bytep	*rows;			// PNG row pointers
    int          w, h, d;
    uchar        *array;


    // Open the PNG file...
    if ((fp = fopen(pngfilename, "rb")) == NULL) {
        errprintf("readPNGimage : cannot open file %s\n", pngfilename);
        return 102;
    }
    // Setup the PNG data structures...
    pp   = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    info = png_create_info_struct(pp);

    if (setjmp(pp->jmpbuf))
    {
        errprintf("PNG file \"%s\" contains errors!\n", pngfilename);
        return 103;
    }

    // Initialize the PNG read "engine"...
    png_init_io(pp, fp);

    // Get the image dimensions and convert to grayscale or RGB...
    png_read_info(pp, info);

    if (info->color_type == PNG_COLOR_TYPE_PALETTE)
        png_set_expand(pp);

    if (info->color_type & PNG_COLOR_MASK_COLOR)
        channels = 3;
    else
        channels = 1;

    if ((info->color_type & PNG_COLOR_MASK_ALPHA) || info->num_trans)
        channels ++;

    w = (int)(info->width);
    h = (int)(info->height);
    d = channels;

    if (info->bit_depth < 8)
    {
        png_set_packing(pp);
        png_set_expand(pp);
    }
    // we ought to read the 16-bit data correctly, since we can !
    else if (info->bit_depth == 16)
        png_set_strip_16(pp);

#  if defined(HAVE_PNG_GET_VALID) && defined(HAVE_PNG_SET_TRNS_TO_ALPHA)
    // Handle transparency...
    if (png_get_valid(pp, info, PNG_INFO_tRNS))
        png_set_tRNS_to_alpha(pp);
#  endif // HAVE_PNG_GET_VALID && HAVE_PNG_SET_TRNS_TO_ALPHA

    array = new uchar[w * h * d];

    // Allocate pointers...
    rows = new png_bytep[h];

    for (i = 0; i < h; i ++)
        rows[i] = (png_bytep)(array + i * w * d);

    // Read the image, handling interlacing as needed...
    for (i = png_set_interlace_handling(pp); i > 0; i --)
        png_read_rows(pp, rows, NULL, h);

#ifdef WIN32
    // Some Windows graphics drivers don't honor transparency when RGB == white
    if (channels == 4) {
        // Convert RGB to 0 when alpha == 0...
        uchar *ptr = (uchar *)array;
        for (i = w * h; i > 0; i --, ptr += 4)
            if (!ptr[3]) ptr[0] = ptr[1] = ptr[2] = 0;
    }
#endif // WIN32

    // Free memory and return...
    delete[] rows;

    png_read_end(pp, info);
    png_destroy_read_struct(&pp, &info, NULL);


    // this will require no translation
    IOBlackBox->setImData(array);
    IOBlackBox->setCurrImgWidth(w);
    IOBlackBox->setCurrImgHeight(h);
    IOBlackBox->setCurrImgThickness(1);
    IOBlackBox->setXOffset(0);
    IOBlackBox->setYOffset(0);
    IOBlackBox->setZOffset(0);
    IOBlackBox->setCurrImgNbComps(1); // only a single image in a PNG file
    IOBlackBox->setCurrImgNbSamples(d);
    IOBlackBox->setCurrImgSpp(d); // needed here because no currBuffp
    IOBlackBox->setCurrPixType(IM_UINT1);
    IOBlackBox->setCurrImgType((d == 1) ? IM_SINGLE : ((d == 3) ? IM_RGB : IM_SPECTRUM));
    IOBlackBox->setImgDesc("PNG");

    
    fclose(fp);

#else
    errprintf("This version of imview cannot read PNG files\n");
    retval = 101; // not supported
#endif // HAVE_PNG && HAVE_LIBZ
    return retval;
}


