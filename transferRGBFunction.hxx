/*
 * $Id: transferRGBFunction.hxx,v 4.0 2003/04/28 14:40:16 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*------------------------------------------------------------------------
 *
 * The long-awaited RGB contrast/brightness/gamma panel for RGB data
 *
 * Hugues Talbot	27 Oct 2001
 *
 *      
 *-----------------------------------------------------------------------*/

#ifndef RGBTRANSFERFUNCTION_H
#define RGBTRANSFERFUNCTION_H

#include <assert.h>
#include <FL/Fl.H>
#include <FL/x.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Slider.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Float_Input.H>
#include <FL/Fl_Int_Input.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Return_Button.H>
#include <FL/Fl_Round_Button.H>
#include <FL/Fl_Choice.H>
#include <FL/Fl_Double_Window.H>
#include "transferRGBFunctionBox.hxx"
#include "transferRGBHistoBox.hxx"
#include "transferRGBBox.hxx"
//#include "transferHistoBox.hxx"


void RGBchosendisplay_cb(Fl_Menu_ *, void *);

class transferRGB {
public:
    transferRGB();
    ~transferRGB();
    void setDefaults();
    void computeDrawingParms();
    void setDrawingParms();
    void redrawWindowIfNeeded();
    void show();
    void hide();
    //
    void setRGammaSliderValue(float val) {RgammaSlider->value(val);}
    void setGGammaSliderValue(float val) {GgammaSlider->value(val);}
    void setBGammaSliderValue(float val) {BgammaSlider->value(val);}
    //
    void setRGammaInputValue(const char *c) {RgammaInput->value(c);}
    void setGGammaInputValue(const char *c) {GgammaInput->value(c);}
    void setBGammaInputValue(const char *c) {BgammaInput->value(c);}
    void setGammaInputValue(int i, const char *c) {assert((i>=0) && (i < 3)); gammaInputs[i]->value(c);}
    //
    void setRContrastSliderValue(float val) {RcontrastSlider->value(val);}
    void setGContrastSliderValue(float val) {GcontrastSlider->value(val);}
    void setBContrastSliderValue(float val) {BcontrastSlider->value(val);}
    //
    void setRBrightnessSliderValue(float val) {RbrightnessSlider->value(val);}
    void setGBrightnessSliderValue(float val) {GbrightnessSlider->value(val);}
    void setBBrightnessSliderValue(float val) {BbrightnessSlider->value(val);}
    // returns which R G & B buttons have been toggled
    void affectChannels(int &R, int &G, int &B) {
	R = Rselect->value();
	G = Gselect->value();
	B = Bselect->value();
    }
    void redrawBox(void) {
	RGBtransferBox->redraw();// redraw the current transfer function or histogram
    }
    // help identify which slider is being modified
    int identifySlider(Fl_Slider *aSlider);
    int identifyInput(Fl_Input *anInput);
    void linkBrightnessSliders(int withId, double v) {linkSliders(brightnessSliders,withId,v);}
    void linkContrastSliders(int withId, double v) {linkSliders(contrastSliders,withId,v);}
    void linkGammaSliders(int withId, double v) {linkSliders(gammaSliders,withId,v);}
    void linkGammaInputs(int withId, char *v) {linkInputs((Fl_Input**)gammaInputs,withId,v);}
    //
    void setTopValue(int t[3]);
    void setBottomValue(int t[3]);
    void setLeftValue(int t[3]);
    void setRightValue(int t[3]);
    void setTransfer();
    void setHisto();
    void getParameters(double &theRGamma, double &theGGamma, double &theBGamma,
		       double &theRContrast, double &theGContrast, double &theBContrast,
		       double &theRBrightness, double &theGBrightness, double &theBBrightness) {
	RGBtransferBox->getRGBParms(theRContrast, theRBrightness, theRGamma,
				    theGContrast, theGBrightness, theGGamma, 
				    theBContrast, theBBrightness, theBGamma);
    }
    void ensureImageRedraw(bool s) {
        RGBtransferBox->setRelease(s);
    }
    friend Fl_Double_Window *RGB_transfer_panel(transferRGB &s);
        
private:
       
    // Fluid-defined
    Fl_Double_Window            *RGBtransferWindow;
    Fl_Group                    *displayBoxGroup;
    Fl_Choice                   *displayChoice;
    Fl_Float_Input              *RgammaInput, *GgammaInput, *BgammaInput;
    Fl_Int_Input                *RtopInput, *RbottomInput, *RleftInput, *RrightInput;
    Fl_Int_Input                *GtopInput, *GbottomInput, *GleftInput, *GrightInput;
    Fl_Int_Input                *BtopInput, *BbottomInput, *BleftInput, *BrightInput;
    myTransferRGBBox            *RGBtransferBox, *RGBmemoryBox;
    myTransferRGBHistoBox       *transferRGBHistoBox;
    myTransferRGBFunctionBox    *transferRGBFunctionBox;
    Fl_Slider                   *RcontrastSlider, *GcontrastSlider, *BcontrastSlider;
    Fl_Slider                   *RbrightnessSlider, *GbrightnessSlider, *BbrightnessSlider;
    Fl_Slider                   *RgammaSlider, *GgammaSlider, *BgammaSlider;
    Fl_Button                   *resetButton, *normaliseButton;
    Fl_Button                   *Rselect, *Gselect, *Bselect;
    Fl_Round_Button             *linkRButton, *linkGButton, *linkBButton;
    Fl_Check_Button             *applyToAllButton;
    Fl_Return_Button            *OKButton;
    // management
    Fl_Slider                   *contrastSliders[3], *brightnessSliders[3], *gammaSliders[3];
    Fl_Round_Button             *linkButtons[3];
    Fl_Int_Input                *topInputs[3], *bottomInputs[3], *leftInputs[3], *rightInputs[3];
    Fl_Float_Input              *gammaInputs[3];
    void linkSliders(Fl_Slider *sliderKind[3], int withId, double v);
    void linkInputs(Fl_Input *inputKind[3], int withId, char *value);
};

#endif // RGBTRANSFERFUNCTION_H
