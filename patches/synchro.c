void synchro(char* proc1, char* proc2)
{
 int df[2];                // descripteurs de tube

 pipe(df);
 if(fork() != 0)
 {
   close(df[0]);   // fermeture en lecture
   dup2(df[1], 1);   // df[1] devient sortie standard stdout (1=stdout)
   close(df[1]);   // df[1] inutile apr�s redirection
   execl(proc1, proc1, 0);
 } else {
   close(df[1]);    // fermeture en �criture
   dup2(df[0],0);  // df[0] devient entr�e standard (0=stdin)
   close(df[0]);    // inutile apr�s redirection
   execl(proc2, proc2, 0);
 }
}
