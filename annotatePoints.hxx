/*
 * $Id: annotatePoints.hxx,v 4.0 2003/04/28 14:39:39 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*------------------------------------------------------------------------
 *
 * Point annotation header file.
 *
 * Hugues Talbot	13 Jan 2001
 *      
 *-----------------------------------------------------------------------*/

#ifndef ANNOTATEPT_H
#define ANNOTATEPT_H

#include "imview.hxx"
#include "imnmspc.hxx" // namespace def. if required

#include <FL/Fl.H>
#include <FL/x.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Scroll.H>
#include <FL/Fl_Choice.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Round_Button.H>
#include <FL/Fl_Return_Button.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Output.H>
#include <FL/Fl_Roller.H>
#include <FL/Fl_Value_Output.H>
#include <FL/Fl_Value_Slider.H>

// the GUI interface
class annotatept {
public:
    annotatept();
    ~annotatept();
    void               setDefaults();
    const char        *getAnnotation(void) {return annotateInput->value();}
    void               setAnnotation(const char *s) { annotateInput->value(s); }
    int                getDistance(void) {return (int)(distanceValue->value());}
    void               setDistance(int v) { distanceValue->value(v);}
    int                getFont(void) {return fontChoice->value();}
    void               setFont(int v) { fontChoice->value(v);}
    int                getFontColour(void) {return fontColourChoice->value();}
    void               setFontColour(int v) {fontColourChoice->value(v);}
    int                getFontSize(void) {return (int)(fontSizeSlider->value());}
    void               setFontSize(int v) {fontSizeSlider->value(v);}
    void               setFontQuadrant(str_quadrant v) {fontQuadrant = v;}
    str_quadrant       getFontQuadrant(void) {return fontQuadrant;}
    void               setXY(int xx, int yy) {X_ = xx; Y_ = yy;}
    void               getXY(int &xx, int &yy) {xx = X_, yy = Y_;}
    void               hide(bool h) {  hiddenButton->value((int)(h));}
    bool               isHidden(void) {return (hiddenButton->value() > 0);}
    int                getPointSize(void) {return ((int)pointsizeValueSlider->value());}
    void               setPointSize(int s) {pointsizeValueSlider->value(s);}
    void               getPointParams(void);
    void               show();
    void               hide();
    friend Fl_Window  *annotatept_panel(annotatept &anpp);

private:
    int                X_, Y_;
    Fl_Window         *annotateWindow;
    Fl_Input          *annotateInput;
    Fl_Button         *CancelButton;
    Fl_Return_Button  *OKButton;
    Fl_Choice         *fontChoice;
    Fl_Roller         *distanceRoller;
    Fl_Value_Output   *distanceValue;
    Fl_Choice         *fontColourChoice;
    Fl_Slider         *fontSizeSlider;
    Fl_Round_Button   *hiddenButton;
    Fl_Value_Slider   *pointsizeValueSlider;
    str_quadrant       fontQuadrant;
    
};

#endif // ANNOTATEPT_H
