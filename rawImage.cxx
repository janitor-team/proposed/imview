/*
 * $Id: rawImage.cxx,v 4.6 2009/01/26 14:39:43 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*------------------------------------------------------------------------
 *
 * A panel for reading arbitrary uncompressed images.
 *
 * Hugues Talbot	 8 Apr 2000
 *
 * A wish from the Perth group.
 *      
 *-----------------------------------------------------------------------*/

#include "imnmspc.hxx"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>
#include <sys/stat.h>
#include <map> // STL
#include <string> // STL strings
#include <iostream>
#include <fstream>
#include <sstream>
#include "imunistd.h"
#include "imview.hxx"
#include "imageIO.hxx"
#include "rawImage.hxx"
#include "menubar.hxx" // for openimage
#include "machine.hxx"

extern imageIO *IOBlackBox;

extern std::map <string, RAWFILE_HEADER> rawfilemap;

// we use a global static array so that the
// callbacks can see it.
static pixtype pixtype_match[NBTYPES] = {
    IM_UINT1, IM_INT2, IM_UINT2,
    IM_INT4, IM_UINT4, IM_INT8,
    IM_UINT8, IM_FLOAT, IM_DOUBLE
};

rawimage::rawimage()
{
    dbgprintf("Raw image panel constructed\n");
    rawimageWindow = 0; // fluid depends on this
    return;
}

rawimage::~rawimage()
{
    // filename is now a char array.
}

void rawimage::setFileName(const char *v) 
{
   strncpy(filename, v, 1023);
}


void rawimage::setDefaults(void)
{
    // explanatory text
    bannerOutput->value("If you know the image data is uncompressed and you know the image\n"
			"dimensions, you can enter them below; otherwise press Cancel.");
    
    xInput->value("1");
    yInput->value("1");
    zInput->value("1");
    tInput->value("1");

    nbBandsInput->value("1");
    bandTypeChoice->deactivate(); // no need if there is only one band
    assignButton->deactivate(); // same

    headerSizeInput->value("0");
    filesizeOutput->value("0");

    // consistent corresponding values
    nx = ny = nz = nt = 1;
    spp = 1;
    interleave = 0; // default from fluid
    byteOrder = 0; // sameg
    ptype = IM_UINT1; // same
    dataSize = headerSize = 0;
    
    dataSize = 0;
    filename[0] = '\0';
}

void rawimage::show()
{
    rebuildBoxTitle();
    rawimageWindow->show();
    loadHeader();
    return;
}

void rawimage::hide()
{
    rawimageWindow->hide();
    return;
}

void rawimage::recomputeHeader(void)
{
    int header;

    header = computeSkip();

    if (header < 0) {
	headerSizeInput->textcolor(FL_RED);
	OKButton->deactivate();
    } else {
	headerSizeInput->textcolor(FL_BLACK);
	OKButton->activate();
    }

    if (spp > 1) {
	bandTypeChoice->activate();
	assignButton->activate();
    } else {
	bandTypeChoice->deactivate();
	assignButton->deactivate();
    }
    
}

int rawimage::computeSkip(void)
{
    int totaldata;
    
    totaldata = nx*ny*nz*nt*spp * IOBlackBox->typeSize(ptype);
    headerSize = dataSize - totaldata;


    return headerSize;
}

void rawimage::setHeaderInput(void)
{
    char buf[100];
    int v = computeSkip();

    snprintf(buf, 100, "%d", v);
    headerSizeInput->value(buf);
}

void rawimage::saveHeader(void)
{
    RAWFILE_HEADER rfh;

    rfh.nx = nx;
    rfh.ny = ny;
    rfh.nz = nz;
	rfh.nt = nt;
    rfh.spp = spp;
    rfh.byo = (endianness) byteOrder;
    rfh.itl = interleave;
    rfh.pixt = ptype;
    rfh.filesize = dataSize;
    rfh.skip = headerSize;
    rfh.externalData = NULL;
    
    rawfilemap[filename] = rfh; 
    // save the header as well
    //
    std::ostringstream headerfilename;
    headerfilename << filename << ".ivh" ; // ImView Header
	
    std::ofstream headerfile(headerfilename.str().c_str());
    if (headerfile) {
        headerfile << "# header file for " << filename << std::endl; 
        headerfile << "nx=" << nx << std::endl;
        headerfile << "ny=" << ny << std::endl;
        headerfile << "nz=" << nz << std::endl;
        headerfile << "nt=" << nt << std::endl;
        headerfile << "spp=" << spp << std::endl;
        headerfile << "byo=" << byteOrder << std::endl;
        headerfile << "itl=" << interleave << std::endl;
        headerfile << "pixt=" << ptype << std::endl;
        headerfile << "filesize=" << dataSize << std::endl;
        headerfile << "skip=" << headerSize << std::endl;
        headerfile << "extrenalData = (null)" << std::endl;
        headerfile.close();
    }
}

bool rawimage::loadHeader(void)
{
    int nbval = 0;
    bool retval = false;
    static char newTitle[100];
    
    std::ostringstream headerfilename;
    headerfilename << filename << ".ivh" ; // ImView Header
	
    std::ifstream headerfile(headerfilename.str().c_str());
    if (headerfile) {
        string currentline;
        string::size_type pos;
        dbgprintf("Loading header for raw image %s\n", filename);
        do {
            getline(headerfile, currentline);

            if (currentline[0] == '#') // comment
                continue; 
            // look for the = sign
            if ((pos = currentline.find("=")) != string::npos) {
                int value = atoi(currentline.substr(pos+1).c_str());
                string key = currentline.substr(0,pos);
                dbgprintf("Key=%s, value = %d\n", key.c_str(), value);
                if (key == "nx") { nx = value; ++nbval ; continue;}
                if (key == "ny") { ny = value; ++nbval ; continue;}
                if (key == "nz") { nz = value ; ++nbval ; continue;}
                if (key == "nt") { nt = value ; ++nbval ; continue;}
                if (key == "spp") {spp = value ; ++nbval ; continue;}
                if (key == "byo") {byteOrder = value; ++nbval ; continue;}
                if (key == "itl") {interleave = value ; ++nbval ; continue;}
                if (key == "pixt") {ptype = static_cast<pixtype>(value); ++nbval ; continue;}
                if (key == "filesize") {dataSize = value ; ++nbval ; continue;}
                if (key == "skip") {headerSize = value ; ++nbval ; continue;}
                dbgprintf("Unknown key !\n");
            } 
        }  while (!currentline.empty());
        headerfile.close();

        dbgprintf("nbval = %d\n", nbval);
        if (nbval == 10) {
            // all fields are filled, save stuff in header.
            RAWFILE_HEADER rfh;

            dbgprintf("Setting values into panel\n");
            rfh.nx = nx; setInputValue(xInput, nx);
            rfh.ny = ny; setInputValue(yInput, ny);
            rfh.nz = nz; setInputValue(zInput, nz);
			rfh.nt = nt; setInputValue(tInput, nt);
            rfh.spp = spp; setInputValue(nbBandsInput, spp);
            // the byte ordering choice is in the right order
            rfh.byo = (endianness) byteOrder; byteOrderChoice->value(byteOrder);
            // the band interleave are in the right order
            rfh.itl = interleave; bandTypeChoice->value(interleave);
            // the pixel types are not in order
            rfh.pixt = ptype; setPixtypeChoice(ptype);
            rfh.filesize = dataSize;
            rfh.skip = headerSize; setInputValue(headerSizeInput, headerSize);
            // not used in this context
            rfh.externalData = NULL;
            
            rawfilemap[filename] = rfh;
            retval = true;
        }
        // Change the dialog's message
        snprintf(newTitle, 100, "%s: RAW format with header found", myBaseName(filename));
        titleBox->label(newTitle);
        titleBox->labelcolor(FL_DARK_GREEN);
    }
    return retval;
}

void rawimage::tryReading(void)
{
    // try reading the file again...
    openimage(filename);
}

// work out the file size as well...
void rawimage::rebuildBoxTitle(void)
{
    static char newTitle[100], fst[20];
    struct stat  statbuf;
    
    assert(filename != 0);
    snprintf(newTitle, 100, "%s: format not recognized", myBaseName(filename));
    titleBox->label(newTitle);
    titleBox->labelcolor(FL_RED);
    
    if (stat(filename, &statbuf) == 0)
	dataSize =  statbuf.st_size;
    else
	dataSize = 0;
    
    snprintf(fst, 20, "%d", dataSize);

    filesizeOutput->value(fst);
    
}
//------- callbacks --------
void xinput_cb(Fl_Int_Input *i, rawimage *panel)
{
    int nx;

    nx = atoi(i->value());
    panel->setnx(nx);
}

void yinput_cb(Fl_Int_Input *i, rawimage *panel)
{
    int ny;

    ny = atoi(i->value());
    panel->setny(ny);
}

void zinput_cb(Fl_Int_Input *i, rawimage *panel)
{
    int nz;

    nz = atoi(i->value());
    panel->setnz(nz);
}

void tinput_cb(Fl_Int_Input *i, rawimage *panel)
{
    int nt;

    nt = atoi(i->value());
    panel->setnt(nt);
}

void nbband_cb(Fl_Int_Input *i, rawimage *panel)
{
    int spp;

    spp = atoi(i->value());
    panel->setspp(spp);
}

void pixeltype_cb(Fl_Choice *c, rawimage *panel)
{
    int rp = c->value();

    dbgprintf("pixel type callback: chosen %d\n", rp);
    assert((rp >= 0) && (rp < NBTYPES));
    panel->setPixType(pixtype_match[rp]);
}

void interleave_cb(Fl_Choice *c, rawimage *panel)
{
    int leav = c->value();

    dbgprintf("Interleave callback: %d\n", leav);
    assert((leav >= 0) && (leav < NBINTER));
    panel->setInterleave(leav);
	   
}

void byteorder_cb(Fl_Choice *c, rawimage *panel)
{
    int bo = c->value();

    dbgprintf("Byte order callback: %d\n", bo);
    assert((bo >= 0) && (bo < NBBYORD));
    panel->setByteOrder(bo);
   
}

void computeskip_cb(Fl_Button *, rawimage *panel)
{
    panel->setHeaderInput();
}

void headersize_cb(Fl_Int_Input *i, rawimage *panel)
{
    int hs = atoi(i->value());
    dbgprintf("Header size changed to %d\n", hs);
    panel->setHeaderSize(hs);
}

void helpbutton_cb(Fl_Button *, rawimage *panel)
{
    dbgprintf("Help on rawimage called\n");
    return;
}

void cancelbutton_cb(Fl_Button *, rawimage *panel)
{
    panel->hide();
}

void okbutton_cb(Fl_Button *, rawimage *panel)
{
    dbgprintf("OK Button pressed, saving the structure... \n");
    panel->saveHeader();
    panel->tryReading();
    //panel->hide();
}

