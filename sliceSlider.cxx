/*
 * $Id: sliceSlider.cxx,v 1.2 2003/05/30 13:02:11 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*------------------------------------------------------------------------
 *
 * A simple linked slider/value panel
 *
 * Hugues Talbot	29 May 2003
 *      
 *-----------------------------------------------------------------------*/

#include "imnmspc.hxx" // name space and everything...

#include <stdio.h>
#include "imview.hxx"
#include "menubar.hxx"
#include "sliceSlider.hxx"


slideinput::slideinput()
{
    dbgprintf("Slide input panel constructed\n");
    // must be done (Fluid depens on it)
    sliceWindow = 0;
    return;
}


void slideinput::setDefaults()
{
    dbgprintf("Setting defaults for slide input\n");
    ((Fl_Valuator*)sliceSlider)->value(0.0);
    minSlice=1;
    maxSlice=1;
    displayslice = 0; // NULL callback
    sliceSlider->step(1.0);
    sliceSlider->linesize(1); // step of the arrow keys
    sliceValue->step(1.0);
}

void slideinput::setLimits(int minslice, int maxslice)
{
    minSlice = minslice;
    maxSlice = maxslice;
    
    sliceSlider->bounds(minslice, maxslice);
    sliceSlider->range(minslice, maxslice);

    sliceValue->bounds(minslice, maxslice);
    sliceValue->range(minslice, maxslice);
}

void slideinput::setSlice(int v)
{
    ((Fl_Valuator*)sliceSlider)->value(v); 
    sliceValue->value(v);
    // expensive call
    if (displayslice)
	displayslice(v);
}

void slideinput::setWindowTitle(const char *title)
{
    sliceWindow->label(title);
}

void slideinput::setValueTitle(const char *title)
{
    sliceValue->label(title);
}

bool slideinput::visible()
{
    return (sliceWindow->visible() != 0);
}

void slideinput::show()
{
    sliceWindow->show();
}

void slideinput::hide()
{
    sliceWindow->hide();
}

void sliceslider_cb(Fl_Scrollbar *i, slideinput *panel)
{
    panel->setSlice(i->value());
}

void slicevalue_cb(Fl_Value_Input *i, slideinput *panel)
{
    panel->setSlice((int)(i->value()));
}

void okbutton_cb(Fl_Return_Button *, slideinput *panel)
{
    dbgprintf("Dismiss button pressed on slide input panel\n");
    panel->hide();
    return;
}
