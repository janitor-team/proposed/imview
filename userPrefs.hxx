/*
 * $Id: userPrefs.hxx,v 4.2 2004/06/21 09:53:14 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*------------------------------------------------------------------------
 *
 * A user preference class
 *
 * Interface with fluid-generated panel
 *
 *      
 *-----------------------------------------------------------------------*/

#ifndef USERPREFS_H
#define USERPREFS_H

#include <FL/Fl.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Round_Button.H>
#include <FL/Fl_Return_Button.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Float_Input.H>
#include <FL/Fl_Int_Input.H>
#include <FL/Fl_Slider.H>
#include <FL/Fl_Tabs.H>
#include <FL/Fl_Tile.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Value_Slider.H>
#include <FL/Fl_Choice.H>

#include <stdio.h>
#include <string>

using std::string;

class userprefs {
public:
    userprefs();
    void setDefaults();
    void setDebug(int isDebugOn) {
	if (isDebugOn)
	    showDebugMsgButton->set();
	else
	    showDebugMsgButton->clear();
    }
    void setStopDebug(int isStopOn) {
	if (isStopOn)
	    stopAfterDebugMsgButton->set();
	else
	    stopAfterDebugMsgButton->clear();
    }
    void setInputHz(double v) {
	char buff[20];
	sprintf(buff, "%g", v);
	pollFrequencyInput->value(buff);
    }
    void setSliderHz(double v) {
	pollFrequencySlider->value(v);
    }
    // access to private values
    const char *getGSPath(void) {return pathToGsInput->value();}
    const char *getPreviewerPath(void) {return pathToPSPreviewer->value();}
    const char *getRenderRes(void) {return renderInput->value();}
    const char *getDisplayRes(void) {return displayInput->value();}
    const char *getBreakXORValue(void) {return xorValueBreakLineInput->value();}
    const char *getProfileXORValue(void) {return xorValueProfileLineInput->value();}

    string getPsPrefsHash(void);
    
    void revertPrefs();
    void savePrefs();
    void show();
    void hide();
    friend Fl_Window *userprefs_panel(userprefs &s);
    
private:

    // the dialog window
    Fl_Window             *userprefsWindow;

    // tabs themselves
    Fl_Group              *generalGroup; 
    // General tab:
    Fl_Input              *pathToPrefsInput;
    Fl_Button             *prefBrowse;
    Fl_Round_Button       *smoothWhenZoomoutButton, *keepPointsButton;
    Fl_Check_Button       *constrainZoomCheck, *unconstrainZoomCheck;
    Fl_Value_Slider       *transparencyValueSlider;
    
    // Postscript Tab:
    Fl_Input              *pathToGsInput;
    Fl_Button             *gsBrowse;
    Fl_Int_Input          *renderInput, *displayInput;
    Fl_Check_Button       *colourCheck, *greyCheck, *bwCheck;
    Fl_Round_Button       *smoothButton, *antialiasButton;
    Fl_Choice             *boundingChoice;
    Fl_Input              *pathToPSPreviewer;
    Fl_Button             *gvBrowse;

    // Server Tab:
    Fl_Float_Input        *pollFrequencyInput;
    Fl_Slider             *pollFrequencySlider;
    Fl_Round_Button       *securityPasswordButton, *securityLocalhostButton;
    
    // Expert tab
    Fl_Round_Button       *hideMenuButton;

    // hack Tab
    Fl_Round_Button       *showDebugMsgButton, *stopAfterDebugMsgButton;
    Fl_Input              *xorValueBreakLineInput, *xorValueProfileLineInput;
    
    
    // the main buttons
    Fl_Return_Button      *OKButton;
    Fl_Button             *saveButton, *cancelButton;
};


#endif // USERPREFS_H
