#  The SPEC file for the imview RPM.
#  this is for version @VERSION@
#
#  Hugues Talbot	 2 Jan 2001
#

Prefix: /usr/X11R6
Buildroot: /home/talbot/projects/GUI/imview/distrib/redhat
Summary: image display and analysis
Name: imview
Version: @VERSION@
Release: 2
Copyright: GPL
Group: X11/Application/Graphics
Source: /home/talbot/projects/GUI/imview/distrib/redhat/SOURCES/imview-src-@VERSION@.tar.gz
URL: http://www.cmis.csiro.au/hugues.talbot/imview
Packager: Hugues Talbot
%description
This application allows the user to display a variety of image formats
and to help analyse the content of images. It can perform as a general-
purpose image display program with some nice features, but it is
designed with the profesionnal image analysis/image processing person 
in mind.

%prep
%setup

%build
CXX=/usr/bin/c++
export CXX
./configure --prefix=/usr/X11R6
make depend
make RPM_OPT_FLAGS="$RPM_OPT_FLAGS" all

%install
make DESTDIR="$RPM_BUILD_ROOT" install-strip

%files
%attr(-, root, root) %doc README COPYING ChangeLog
%attr(-, root, root) %dir /usr/X11R6/share/Imview
%attr(-, root, root) %dir /usr/X11R6/share/Imview/doc
%attr(-, root, root) /usr/X11R6/bin/imview
%attr(-, root, root) /usr/X11R6/share/Imview/NeWSfb.lut
%attr(-, root, root) /usr/X11R6/share/Imview/binary.lut
%attr(-, root, root) /usr/X11R6/share/Imview/blue.lut
%attr(-, root, root) /usr/X11R6/share/Imview/green.lut
%attr(-, root, root) /usr/X11R6/share/Imview/heat.lut
%attr(-, root, root) /usr/X11R6/share/Imview/hue.lut
%attr(-, root, root) /usr/X11R6/share/Imview/inv_binary.lut
%attr(-, root, root) /usr/X11R6/share/Imview/inv_grey.lut
%attr(-, root, root) /usr/X11R6/share/Imview/inv_heat.lut
%attr(-, root, root) /usr/X11R6/share/Imview/inv_rainbow.lut
%attr(-, root, root) /usr/X11R6/share/Imview/inv_spec.lut
%attr(-, root, root) /usr/X11R6/share/Imview/inv_spiral.lut
%attr(-, root, root) /usr/X11R6/share/Imview/overlay_blue.lut
%attr(-, root, root) /usr/X11R6/share/Imview/overlay_cyan.lut
%attr(-, root, root) /usr/X11R6/share/Imview/overlay_green.lut
%attr(-, root, root) /usr/X11R6/share/Imview/overlay_magenta.lut
%attr(-, root, root) /usr/X11R6/share/Imview/overlay_red.lut
%attr(-, root, root) /usr/X11R6/share/Imview/overlay_yellow.lut
%attr(-, root, root) /usr/X11R6/share/Imview/rainbow.lut
%attr(-, root, root) /usr/X11R6/share/Imview/ramps.lut
%attr(-, root, root) /usr/X11R6/share/Imview/red.lut
%attr(-, root, root) /usr/X11R6/share/Imview/regions.lut
%attr(-, root, root) /usr/X11R6/share/Imview/regions1.lut
%attr(-, root, root) /usr/X11R6/share/Imview/rg.lut
%attr(-, root, root) /usr/X11R6/share/Imview/rgb.lut
%attr(-, root, root) /usr/X11R6/share/Imview/spectrum.lut
%attr(-, root, root) /usr/X11R6/share/Imview/spiral.lut
%attr(-, root, root) /usr/X11R6/share/Imview/std_grey.lut
%attr(-, root, root) /usr/X11R6/share/Imview/imview_banner.jpg
%attr(-, root, root) /usr/X11R6/share/Imview/doc/quickhelp.html
%attr(-, root, root) /usr/X11R6/share/Imview/doc/quickhelp-1.html
%attr(-, root, root) /usr/X11R6/share/Imview/doc/quickhelp-2.html
%attr(-, root, root) /usr/X11R6/share/Imview/doc/quickhelp-3.html
%attr(-, root, root) /usr/X11R6/share/Imview/doc/quickhelp-4.html
%attr(-, root, root) /usr/X11R6/share/Imview/doc/warranty.html
%attr(-, root, root) /usr/X11R6/share/Imview/doc/gpl.html
%attr(-, root, root) /usr/X11R6/man/man1/imview.1.gz
