/*
 * $Id: view3d.cxx,v 4.1 2005/03/03 14:45:35 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2005 by the Australian Commonwealth
 *  Science and Industry Research Organisation (CSIRO). Please see the
 *  COPYRIGHT file for full details. Imview also includes the
 *  contributions of many others. Please see the CREDITS file for full
 *  details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*------------------------------------------------------------------------
 *
 *  3D view widget. Allows 3 views of a 3d object (or time series, or
 *  hyperspectral image) at the same time. This is fairly standard in
 *  medical imaging. The innovation here might be the zoom.
 *
 *  Hugues Talbot	 2 Mar 2005
 *
 *-----------------------------------------------------------------------*/

#include "imnmspc.hxx"
#include "imview.hxx"
#include "view3d.hxx"


view3D::view3D()
{
    dbgprintf("3D view panel constructed\n");
    view3dWindow = 0; // fluid depends on that
    return;
}

void view3D::setDefaults()
{
    return;
}


// pass the message along
void view3D::show()
{
    view3dWindow->show();
    return;
}

void view3D::hide()
{
    view3dWindow->hide();
    return;
}
